# Hyperstack Material UI Starter

## Introduction

This document will guide you through creating a [Hyperstack](https://hyperstack.org) app with [Material UI](https://material-ui.com).

## Setting Up

Firstly, let's generate a Rails app. 

At the time of writing, Hyperstack works with Rails 5 and has not yet been tested against Rails 6. 

If you have Rails 6 installed you can use Rails 5 to generate your app like this:

    rails _5.2.3_ new hyperstack_material_ui_starter --skip-test

of course, if you don't have anything later than Rails 5 installed, just use the rails command as normal:

    rails new hyperstack_material_ui_starter --skip-test

and then

    cd hyperstack_material_ui_starter

Run a bundler command to install the `rails-hyperstack` gem into the system and add it to your app.

    bundle add 'rails-hyperstack' --version "~> 1.0.alpha1.0"

Once the gem is installed run

    bundle exec rails hyperstack:install


Finally find the `config/initializers/hyperstack.rb` file, and make sure that this line is not commented out:

    Hyperstack.import 'hyperstack/component/jquery', client_only: true

Ignore any comments saying that it should be commented out, this is a typo in the current installer.

Start up the app with `foreman start` and point a browser to <http://127.0.0.1:5000/>. 

You should see a page with "App" on it. 

## Adding Material UI

Install the Material UI core library with yarn:

    yarn add @material-ui/core

Now import an object from Material UI to confirm it's installed properly. 

In `app/javascript/packs/client_and_server.js` add this require command at the bottom

```ruby
Button = require('@material-ui/core/Button');
```

In `app/hyperstack/components/app.rb` add the following code to after the `DIV` block line

```ruby
Button(variant: :contained, color: :primary) { "Click me" }.on(:click) do
  alert 'you clicked the button!'
end
```

So the whole file should now appear as:


```ruby
class App < HyperComponent
  include Hyperstack::Router
  render do
    DIV do
      'App'
      # define routes using the Route psuedo component.  Examples:
      # Route('/foo', mounts: Foo)                : match the path beginning with /foo and mount component Foo here
      # Route('/foo') { Foo(...) }                : display the contents of the block
      # Route('/', exact: true, mounts: Home)     : match the exact path / and mount the Home component
      # Route('/user/:id/name', mounts: UserName) : path segments beginning with a colon will be captured in the match param
      # see the hyper-router gem documentation for more details
    end
    Button(variant: :contained, color: :primary) { "Click me" }.on(:click) do
      alert 'you clicked the button!'
    end
  end
end
```

Recomplie with webpacker and restart the app. 

    bin/webpack
    foreman start

Note: If you're a little paranoid like me, you can clobber the compiled assets and then recompile using the rails rake tasks

    rake webpacker:clobber ; rake webpacker:compile
    foreman start

Now, you should see a nice Material UI button on your page.

To add all of Material UI add the following to your `app/javascript/packs/client_and_server.js` file.

```ruby
Mui = require('@material-ui/core');
Mui.default = {};
```

The `Mui.default = {};` is required due to a bug where a prop is not being set, usually resulting in an error message like


    Uncaught error: TypeError: Cannot read property 'prototype' of undefined
    in App (created by Hyperstack::Internal::Component::TopLevelRailsComponent)
    in Hyperstack::Internal::Component::TopLevelRailsComponent

*Thanks to Barrie Hadfield for figuring this one out*

To try that out, let's use the Material Typography component. Before the `DIV` block in `app/hyperstack/components/app.rb`, add

```ruby
Mui.Typography(:variant => 'h2') do
  'My Application'
end
```

Now the file looks like:

```ruby
class App < HyperComponent
  include Hyperstack::Router
  render do
    Mui.Typography(:variant => 'h2') do
      'My Application'
    end
    DIV do
      'App'
      # define routes using the Route psuedo component.  Examples:
      # Route('/foo', mounts: Foo)                : match the path beginning with /foo and mount component Foo here
      # Route('/foo') { Foo(...) }                : display the contents of the block
      # Route('/', exact: true, mounts: Home)     : match the exact path / and mount the Home component
      # Route('/user/:id/name', mounts: UserName) : path segments beginning with a colon will be captured in the match param
      # see the hyper-router gem documentation for more details
    end
    Button(variant: :contained, color: :primary) { "Click me" }.on(:click) do
      alert 'you clicked the button!'
    end
  end
end
```